package com.gitlab.cvazer.surrogate.shared.model

import java.util.UUID

data class Tile(
    var id: String = UUID.randomUUID().toString(),
)