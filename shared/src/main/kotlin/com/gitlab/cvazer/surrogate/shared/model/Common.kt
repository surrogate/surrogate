package com.gitlab.cvazer.surrogate.shared.model

data class Pos(var x: Int = 0, var y: Int = 0)