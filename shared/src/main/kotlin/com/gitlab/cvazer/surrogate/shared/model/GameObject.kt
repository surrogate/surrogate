package com.gitlab.cvazer.surrogate.shared.model

import java.util.*

data class GameObject(
    val id: String = UUID.randomUUID().toString(),
    var layer: Int = 0,
    var level: Level = Level.FLOOR,
    val pos: Pos = Pos(),
    var atomLocator: String,
    val props: MutableMap<String, String>
) {
    enum class Level {
        FLOOR
    }
}