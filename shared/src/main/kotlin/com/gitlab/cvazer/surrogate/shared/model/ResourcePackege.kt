package com.gitlab.cvazer.surrogate.shared.model

data class ResourcePackage(val path: String, val fileName: String, val base64: String){
    fun location() = Resource.resLocation(this)
}