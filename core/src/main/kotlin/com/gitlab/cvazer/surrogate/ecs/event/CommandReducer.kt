package com.gitlab.cvazer.surrogate.ecs.event

annotation class CommandReducer(val command: String)
