package com.gitlab.cvazer.surrogate.game.atom

@Target(AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
annotation class TargetingAtoms(vararg val targets: String)
