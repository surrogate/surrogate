package com.gitlab.cvazer.surrogate.game.atom.strategies.creation

import com.badlogic.ashley.core.Engine
import com.badlogic.ashley.core.Entity
import com.github.czyzby.autumn.annotation.Component
import com.github.czyzby.autumn.annotation.Inject
import com.gitlab.cvazer.surrogate.ecs.ComponentListeningEngine
import com.gitlab.cvazer.surrogate.ecs.EcsSystemResourceRegistry
import com.gitlab.cvazer.surrogate.ecs.ListenedEntity
import com.gitlab.cvazer.surrogate.ecs.components.*
import com.gitlab.cvazer.surrogate.game.atom.TargetingAtoms
import com.gitlab.cvazer.surrogate.game.atom.types.EntityStrategy
import com.gitlab.cvazer.surrogate.shared.model.Pawn
import com.gitlab.cvazer.surrogate.system.AtomsLoadingSystem
import ktx.ashley.get
import ktx.ashley.mapperFor

@Component
@TargetingAtoms("pawn.?")
class PawnEntityStrategy: EntityStrategy<Pawn> {
    @Inject private lateinit var ecsSystemResourceRegistry: EcsSystemResourceRegistry
    @Inject private lateinit var atomsLoadingSystem: AtomsLoadingSystem

    private val worldRendering = mapperFor<WorldRendering>()

    override fun toEntity(target: Pawn, parent: Entity?, engine: Engine?): List<Entity> {
        if (engine == null) return emptyList()
        if (engine !is ComponentListeningEngine) return emptyList()

        val zLevel = parent
            ?.let { it[worldRendering] }
            ?.zLevel
            ?: 0

        val entity = ListenedEntity(engine)
            .add { Id(target.id) }
            .add { Parent(parent) }
            .add { Position(target.pos.x, target.pos.y) }
            .add { Transform() }
            .add { Physics() }
            .add { AtomComp(target.atomLocator) }
            .add { Props(target.props.toMutableMap()) }
            .add { WorldRendering(zLevel + 1) }

        if (target.id == ecsSystemResourceRegistry["charId"]) {
            ecsSystemResourceRegistry["charEntity"] = entity
            entity.add { Player() }
        }

        return listOf(entity)
    }
}