package com.gitlab.cvazer.surrogate.backend

import com.gitlab.cvazer.surrogate.shared.model.Atom
import com.gitlab.cvazer.surrogate.shared.model.Grid
import com.gitlab.cvazer.surrogate.shared.model.Resource
import com.gitlab.cvazer.surrogate.shared.model.ResourcePackage

interface BackendApi {
    fun loadAccountData(key: String)
    fun loadCharData(id: String)
    fun loadGrid(gridId: String): Grid
    fun loadAtom(locator: String): Atom
    fun loadResource(res: Resource): ResourcePackage
    fun loadCurrentChar(): String?
}