package com.gitlab.cvazer.surrogate.system.service

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.assets.AssetDescriptor
import com.badlogic.gdx.assets.AssetManager
import com.github.czyzby.autumn.annotation.Component
import com.github.czyzby.autumn.annotation.Destroy
import com.gitlab.cvazer.surrogate.shared.model.Resource
import com.gitlab.cvazer.surrogate.shared.model.ResourcePackage
import ktx.assets.loadAsset
import java.util.*

@Component
class AssetManagerService {
    val assetManager = AssetManager()

    val assets = mutableMapOf<String, AssetDescriptor<*>>()

    fun isResourceAvailable(res: Resource): Boolean =
        Gdx.files.local(res.location()).exists()

    fun isAssetForResourceLoaded(res: Resource) = assets.containsKey(res.location())
        && assetManager.isLoaded(assets[res.location()]!!)

    fun saveResourcePackage(pack: ResourcePackage) = Gdx.files.local(pack.location())
        .writeBytes(Base64.getDecoder().decode(pack.base64), false)

    fun loadAsset(res: Resource) {
        assets.getOrPut(res.location()) { AssetDescriptor(Gdx.files.internal(res.location()), Class.forName(res.clazz)) }
            .takeIf { !assetManager.isLoaded(it) }
            ?.apply { assetManager.loadAsset(this) }
    }

    inline fun <reified T> getAsset(res: Resource): T = getAsset(res.location())
    inline fun <reified T> getAsset(location: String): T = assetManager.get(assets[location]) as T

    @Destroy fun dispose() {
        assetManager.dispose()
    }
}