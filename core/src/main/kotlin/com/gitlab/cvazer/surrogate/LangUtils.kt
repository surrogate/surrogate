package com.gitlab.cvazer.surrogate

import com.fasterxml.jackson.module.kotlin.readValue
import ktx.log.Logger
import ktx.log.logger
import kotlin.reflect.KClass

//Logger storage. Call log() to get class specific logger from any class
val loggers = mutableMapOf<KClass<*>, Logger>()
inline fun <reified T: Any> T.log(): Logger = loggers.getOrPut(this::class) { logger<T>() }
inline fun <reified T: Any> T.log(text: String) = log().info { text }

//Context component fetcher. Fetch any autumn @Component from context. Auto casting
inline fun <reified T> getBean(clazz: Class<T>): T = Application.context.getComponent(clazz) as T
inline fun <reified T : Any> getBean(clazz: KClass<T>): T = Application.context.getComponent(clazz.java) as T
inline fun <reified T> getBean(): T = getBean(T::class.java)

//For collecting to non-vanilla maps
inline fun <T, M> Iterable<T>.associateUsing(destination: M, append: (M, T) -> Unit): M {
    for (element in this) {
        append(destination, element)
    }
    return destination
}

//Jackson helper methods
fun <T> String.readAs(clazz: Class<T>): T = Application.objectMapper.readValue(this, clazz)
fun <T> T.jsonString(): String = Application.objectMapper.writeValueAsString(this)
inline fun <reified T> String.readAs(): T = Application.objectMapper.readValue(this)
