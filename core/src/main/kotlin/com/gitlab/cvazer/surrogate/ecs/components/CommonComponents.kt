package com.gitlab.cvazer.surrogate.ecs.components

import com.badlogic.ashley.core.Component
import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.physics.box2d.Body
import com.badlogic.gdx.physics.box2d.World
import com.gitlab.cvazer.surrogate.getBean
import com.gitlab.cvazer.surrogate.shared.model.Atom
import com.gitlab.cvazer.surrogate.system.AtomsLoadingSystem
import java.util.*

data class Id(var uuid: String = UUID.randomUUID().toString()): Component
data class Transform(var x: Float = 0f, var y: Float = 0f): Component
data class AtomComp(val locator: String): Component
data class Meta(val data: MutableMap<String, *>): Component
data class Position(var x: Int, var y: Int): Component
data class Props(val data: MutableMap<String, Any>): Component
data class WorldRendering(val zLevel: Int): Component
data class Physics(var body: Body? = null): Component
data class Parent(val entity: Entity? = null): Component
data class GridWorld(var world: World? = null): Component

//Markers
class Player: Component

fun AtomComp.isNotReady() = !isReady()
fun AtomComp.isReady() = getBean<AtomsLoadingSystem>().isAtomReady(this.locator)
fun AtomComp.getOrNull() = getBean<AtomsLoadingSystem>().getAtom(this.locator)
fun AtomComp.get() = getBean<AtomsLoadingSystem>().getAtom(this.locator)!!

fun Transform.set(x: Float = 0f, y: Float = 0f): Transform = this.apply { this.x = x; this.y = y }

fun Props.getAllProps(atom: Atom) = mutableMapOf<String, Any>()
    .apply { putAll(atom.atomProps) }
    .apply { putAll(data) }

@Suppress("UNCHECKED_CAST")
fun <T> Props.get(key: String): T?  = this.data[key] as T