package com.gitlab.cvazer.surrogate.game.atom.strategies.rendering

import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.github.czyzby.autumn.annotation.Component
import com.github.czyzby.autumn.annotation.Inject
import com.gitlab.cvazer.surrogate.ecs.components.Props
import com.gitlab.cvazer.surrogate.ecs.components.Transform
import com.gitlab.cvazer.surrogate.game.GameUtils
import com.gitlab.cvazer.surrogate.game.atom.TargetingAtoms
import com.gitlab.cvazer.surrogate.game.atom.types.RenderingStrategy
import com.gitlab.cvazer.surrogate.game.getAllProps
import com.gitlab.cvazer.surrogate.shared.model.Atom
import com.gitlab.cvazer.surrogate.shared.model.Resource
import com.gitlab.cvazer.surrogate.system.service.AssetManagerService

@Component
@TargetingAtoms("object.?")
class ObjectRenderingStrategy: RenderingStrategy {
    @Inject private lateinit var assetManagerService: AssetManagerService

    override fun render(batch: SpriteBatch, atom: Atom, transform: Transform, propsComp: Props?, entity: Entity?) {
        val props = getAllProps(atom, propsComp)

        val width = (props["WIDTH"] as Int? ?: 1) * GameUtils.PIXELS_PER_UNIT
        val height = (props["HEIGHT"] as Int? ?: 1) * GameUtils.PIXELS_PER_UNIT

        val textures = atom.resources
            .filter { it.type == Resource.Type.TEXTURE }
            .map { assetManagerService.getAsset<Texture>(it.location()) }
        textures.forEach { t -> batch.draw(
            t,
            transform.x - width / 2,
            transform.y - height / 2,
            width, height
        ) }
    }
}