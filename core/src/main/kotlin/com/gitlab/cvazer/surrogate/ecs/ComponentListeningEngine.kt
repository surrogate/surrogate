package com.gitlab.cvazer.surrogate.ecs

import com.badlogic.ashley.core.Engine
import com.badlogic.ashley.core.Entity

class ComponentListeningEngine: Engine() {

    fun componentWasAdded(entity: Entity) = systems
        .filterIsInstance(LightEntitySystem::class.java)
        .forEach { it.notifyComponentWasAdded(entity) }

    fun componentWasRemoved(entity: Entity) = systems
        .filterIsInstance(LightEntitySystem::class.java)
        .forEach { it.notifyComponentWasRemoved(entity) }

    override fun createEntity(): ListenedEntity = ListenedEntity(this)
}