package com.gitlab.cvazer.surrogate

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.assets.AssetDescriptor
import com.badlogic.gdx.assets.AssetManager
import com.badlogic.gdx.assets.loaders.SkinLoader
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.BitmapFont
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator
import com.badlogic.gdx.scenes.scene2d.ui.Skin
import com.badlogic.gdx.utils.Array
import com.badlogic.gdx.utils.ObjectMap
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import com.github.czyzby.autumn.annotation.Inject
import com.github.czyzby.autumn.context.Context
import com.github.czyzby.autumn.context.ContextDestroyer
import com.github.czyzby.autumn.context.ContextInitializer
import com.github.czyzby.autumn.scanner.ClassScanner
import com.github.czyzby.kiwi.util.gdx.AbstractApplicationListener
import com.github.czyzby.kiwi.util.gdx.asset.Disposables
import com.gitlab.cvazer.surrogate.backend.LocalFilesBackendApi
import com.gitlab.cvazer.surrogate.ecs.EcsSystemResourceRegistry
import com.gitlab.cvazer.surrogate.game.GameInputProcessor
import com.gitlab.cvazer.surrogate.game.stages.PlayGameStage
import com.gitlab.cvazer.surrogate.system.utils.ContextPostProcessor
import com.kotcrab.vis.ui.VisUI
import ktx.assets.load
import ktx.freetype.generateFont
import ktx.scene2d.Scene2DSkin
import kotlin.reflect.full.allSuperclasses
import kotlin.reflect.full.memberProperties
import kotlin.reflect.jvm.isAccessible

class Application(private val scanner: ClassScanner) : AbstractApplicationListener() {
    @Inject private lateinit var orchestrator: Orchestrator
    @Inject private lateinit var localFilesBackendApi: LocalFilesBackendApi
    @Inject private lateinit var gameInputProcessor: GameInputProcessor
    @Inject private lateinit var ecsSystemResourceRegistry: EcsSystemResourceRegistry

    private lateinit var destroyer: ContextDestroyer
    private lateinit var uiAssetManager: AssetManager

    override fun create() {
        //Setup autumn DI context
        destroyer = ContextInitializer()
            .scan(Application::class.java, scanner)
            .clearContextAfterInitiation(false)
            .createMissingDependencies(false)
            .addComponent(this)
            .doAfterInitiation { context = it }
            .initiate()
        postProcessContext()

        //Setup Jackson
        objectMapper = ObjectMapper()
            .configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false)
            .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
            .setSerializationInclusion(JsonInclude.Include.NON_NULL)
            .registerKotlinModule()

        //Setup VisUI
        val generator = FreeTypeFontGenerator(Gdx.files.internal("ui/game-font.ttf"))
        val skinParams = SkinLoader.SkinParameter::class.java.constructors[2]
            .newInstance(
                listOf(8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28, 30, 32, 34, 36, 38, 40, 42, 44, 46)
                    .map { Pair("game-font-${it}", generator.generateFont { size = it }) }
                    .associateUsing(ObjectMap<String, BitmapFont>()) { acc, e -> acc.put(e.first, e.second) }
                    .apply { this.put("default-font", generator.generateFont { size = 12 }) }
            ) as SkinLoader.SkinParameter
        val descriptor = AssetDescriptor("ui/uiskin.json", Skin::class.java, skinParams)
        uiAssetManager = AssetManager()
        uiAssetManager.load(descriptor)
        uiAssetManager.load<Texture>("cursor.png")
        uiAssetManager.finishLoading()
        ecsSystemResourceRegistry.addResource(
            uiAssetManager.get<Texture>("cursor.png"),
            "cursorTexture",
        )
        VisUI.load(uiAssetManager.get(descriptor))
        Scene2DSkin.defaultSkin = VisUI.getSkin()

//        Gdx.input.isCursorCatched = true
//        Gdx.graphics.setVSync(true)
        Gdx.input.inputProcessor = gameInputProcessor

        //Setup orchestrator
        orchestrator.backendApi = localFilesBackendApi
        orchestrator.setGameState(PlayGameStage::class.java)
    }

    override fun resize(width: Int, height: Int) = orchestrator.resize(width, height)

    public override fun render(deltaTime: Float) = orchestrator.tick(deltaTime)

    override fun dispose() {
        VisUI.dispose()
        Disposables.disposeOf(destroyer)
    }

    private fun postProcessContext() {
        val all = getAllBeansFromContext()
        val contextPostProcessors = all
            .filter { it::class.allSuperclasses.contains(ContextPostProcessor::class) }
            .map { it as ContextPostProcessor }
        all.forEach { bean -> contextPostProcessors.forEach { it.processBean(bean::class, bean) } }
    }

    companion object {
        @JvmStatic lateinit var context: Context
        @JvmStatic lateinit var objectMapper: ObjectMapper

        @Suppress("GDXKotlinUnsafeIterator")
        fun getAllBeansFromContext(): List<Any> {
            val mapField = context::class.memberProperties
                .find { it.name == "components" }!!
            mapField.isAccessible = true
            val map = mapField.call(context)
            mapField.isAccessible = false
            return (map as ObjectMap<*,*>).values().flatMap { it as Array<*> }
        }
    }
}