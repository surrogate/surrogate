package com.gitlab.cvazer.surrogate.game.atom.strategies.creation

import com.badlogic.ashley.core.Engine
import com.badlogic.ashley.core.Entity
import com.github.czyzby.autumn.annotation.Component
import com.gitlab.cvazer.surrogate.ecs.ComponentListeningEngine
import com.gitlab.cvazer.surrogate.ecs.ListenedEntity
import com.gitlab.cvazer.surrogate.ecs.components.*
import com.gitlab.cvazer.surrogate.game.atom.TargetingAtoms
import com.gitlab.cvazer.surrogate.game.atom.types.EntityStrategy
import com.gitlab.cvazer.surrogate.shared.model.GameObject
import ktx.ashley.get
import ktx.ashley.mapperFor

@Component
@TargetingAtoms("object.?")
class GameObjectEntityStrategy: EntityStrategy<GameObject> {
    private val worldRendering = mapperFor<WorldRendering>()

    override fun toEntity(target: GameObject, parent: Entity?, engine: Engine?): List<Entity> {
        if (engine == null) return emptyList()
        if (engine !is ComponentListeningEngine) return emptyList()

        val zLevel = parent
            ?.let { it[worldRendering] }
            ?.zLevel
            ?: 0

        return listOf(
            ListenedEntity(engine)
                .add { Id(target.id) }
                .add { Parent(parent) }
                .add { Position(target.pos.x, target.pos.y) }
                .add { Transform() }
                .add { Physics() }
                .add { Props(target.props.toMutableMap()) }
                .add { AtomComp(target.atomLocator) }
                .add { WorldRendering(zLevel + 0) }
                .add { Meta(mutableMapOf(
                    Pair("layer", target.layer),
                    Pair("level", target.level)
                )) }
        )
    }
}