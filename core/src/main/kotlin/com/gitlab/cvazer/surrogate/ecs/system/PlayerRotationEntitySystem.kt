package com.gitlab.cvazer.surrogate.ecs.system

import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.core.Family
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.math.Vector2
import com.github.czyzby.autumn.annotation.Component
import com.gitlab.cvazer.surrogate.ecs.LightEntitySystem
import com.gitlab.cvazer.surrogate.ecs.components.AtomComp
import com.gitlab.cvazer.surrogate.ecs.components.Player
import com.gitlab.cvazer.surrogate.ecs.components.Props
import ktx.ashley.allOf
import ktx.ashley.get
import ktx.ashley.mapperFor
import kotlin.math.sign

@Component
class PlayerRotationEntitySystem: LightEntitySystem() {
    private val propsMapper = mapperFor<Props>()

    override fun updateEntity(entity: Entity, delta: Float) {

        //-1 LEFT 1 RIGHT
        val isLeftOfA = leftOrRightOf(
            Vector2(0f, Gdx.graphics.height.toFloat()),
            Vector2(Gdx.graphics.width.toFloat(), 0f),
            Vector2(Gdx.input.x.toFloat(), Gdx.input.y.toFloat())
        ) <= 0

        val isLeftOfB = leftOrRightOf(
            Vector2(Gdx.graphics.width.toFloat(), Gdx.graphics.height.toFloat()),
            Vector2(0f, 0f),
            Vector2(Gdx.input.x.toFloat(), Gdx.input.y.toFloat())
        ) <= 0

        if (isLeftOfA && isLeftOfB) entity[propsMapper]!!.data["FACING_DIRECTION"] = "LEFT"
        else if (isLeftOfA && !isLeftOfB) entity[propsMapper]!!.data["FACING_DIRECTION"] = "BACK"
        else if (!isLeftOfA && !isLeftOfB) entity[propsMapper]!!.data["FACING_DIRECTION"] = "RIGHT"
        else if (!isLeftOfA && isLeftOfB) entity[propsMapper]!!.data["FACING_DIRECTION"] = "FRONT"
    }

    private fun leftOrRightOf(start: Vector2, end: Vector2, point: Vector2) =
        sign(((end.x - start.x)*(point.y - start.y) - (end.y - start.y)*(point.x - start.x)))


    override fun getFamilyBuilder(): Family.Builder = allOf(
        Player::class, AtomComp::class, Props::class
    )
}