package com.gitlab.cvazer.surrogate.game.atom.strategies.rendering

import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.graphics.g2d.TextureAtlas
import com.github.czyzby.autumn.annotation.Component
import com.github.czyzby.autumn.annotation.Inject
import com.gitlab.cvazer.surrogate.ecs.components.Props
import com.gitlab.cvazer.surrogate.ecs.components.Transform
import com.gitlab.cvazer.surrogate.ecs.components.get
import com.gitlab.cvazer.surrogate.game.GameUtils
import com.gitlab.cvazer.surrogate.game.atom.TargetingAtoms
import com.gitlab.cvazer.surrogate.game.atom.types.RenderingStrategy
import com.gitlab.cvazer.surrogate.shared.model.Atom
import com.gitlab.cvazer.surrogate.shared.model.Resource
import com.gitlab.cvazer.surrogate.system.service.AssetManagerService

@Component
@TargetingAtoms("pawn.*.human.?")
class HumanRenderingStrategy: RenderingStrategy {
    @Inject private lateinit var assetManagerService: AssetManagerService

    private val regionCache = mutableMapOf<String, TextureAtlas.AtlasRegion>()

    private val facingToPrefix = mutableMapOf(
        Pair("FRONT", "front"),
        Pair("BACK", "back"),
        Pair("LEFT", "side"),
        Pair("RIGHT", "side"),
    )

    override fun render(batch: SpriteBatch, atom: Atom, transform: Transform, propsComp: Props?, entity: Entity?) {
        propsComp ?: return
        val direction = (propsComp.data["FACING_DIRECTION"] ?: return) as String

        val headRegionName = "${(propsComp.data["HEAD_TYPE"] ?: return) as String}-${facingToPrefix[direction]}"
        val bodyRegionName = "${(propsComp.data["BODY_TYPE"] ?: return) as String}-${facingToPrefix[direction]}"

        val headRegion = regionCache
            .getOrPut(headRegionName) { cacheAtlasRegion(headRegionName, "head-atlas", atom) ?: return }

        val bodyRegion = regionCache
            .getOrPut(bodyRegionName) { cacheAtlasRegion(bodyRegionName, "body-atlas", atom) ?: return }

        val prevColor = batch.color.cpy()
        propsComp.get<String>("SKIN_COLOR")?.also {
            batch.color = Color.valueOf(it)
        }

        val getHeadOffset: (direction: String) -> Float = {when (it) {
            "LEFT" -> { -5f }
            "RIGHT" -> { 5f }
            else -> { 0f }
        }}

        if (direction == "LEFT") { bodyRegion.flip(true, false); headRegion.flip(true, false) }
        batch.draw(
            bodyRegion,
            transform.x - GameUtils.GRID_UNIT_TO_PIXELS / 2,
            transform.y - GameUtils.GRID_UNIT_TO_PIXELS / 2,
            GameUtils.GRID_UNIT_TO_PIXELS, GameUtils.GRID_UNIT_TO_PIXELS
        )

        batch.draw(
            headRegion,
            transform.x - GameUtils.GRID_UNIT_TO_PIXELS / 2 + getHeadOffset(direction),
            transform.y - GameUtils.GRID_UNIT_TO_PIXELS / 2 + GameUtils.GRID_UNIT_TO_PIXELS/4,
            GameUtils.GRID_UNIT_TO_PIXELS, GameUtils.GRID_UNIT_TO_PIXELS
        )
        batch.color = prevColor
        if (direction == "LEFT") { bodyRegion.flip(true, false); headRegion.flip(true, false) }
    }

    private fun cacheAtlasRegion(regionName: String, resName: String, atom: Atom): TextureAtlas.AtlasRegion? {
        val res = atom.resources.find { it.name == resName && it.type == Resource.Type.ATLAS } ?: return null
        val atlas = assetManagerService.getAsset<TextureAtlas>(res)
        val region = atlas.findRegion(regionName)
        regionCache[regionName] = region
        return region
    }

}