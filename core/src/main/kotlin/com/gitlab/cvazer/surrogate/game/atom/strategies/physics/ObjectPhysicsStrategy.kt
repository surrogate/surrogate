package com.gitlab.cvazer.surrogate.game.atom.strategies.physics

import com.badlogic.gdx.physics.box2d.Body
import com.badlogic.gdx.physics.box2d.BodyDef
import com.badlogic.gdx.physics.box2d.World
import com.github.czyzby.autumn.annotation.Component
import com.github.czyzby.autumn.annotation.Inject
import com.gitlab.cvazer.surrogate.ecs.components.AtomComp
import com.gitlab.cvazer.surrogate.ecs.components.Position
import com.gitlab.cvazer.surrogate.ecs.components.Props
import com.gitlab.cvazer.surrogate.game.PhysicsCategories.FLOOR
import com.gitlab.cvazer.surrogate.game.atom.TargetingAtoms
import com.gitlab.cvazer.surrogate.game.atom.types.PhysicsStrategy
import com.gitlab.cvazer.surrogate.game.getAllProps
import com.gitlab.cvazer.surrogate.system.AtomsLoadingSystem
import ktx.box2d.body
import ktx.box2d.box
import ktx.box2d.filter

@Component
@TargetingAtoms("object.?")
class ObjectPhysicsStrategy: PhysicsStrategy {
    @Inject private lateinit var atomsLoadingSystem: AtomsLoadingSystem

    override fun create(world: World, positionComp: Position, atomComp: AtomComp, propsComp: Props?): Body {
        val props = getAllProps(atomComp, propsComp)

        val width = (props["WIDTH"] ?: 1) as Int
        val height = (props["HEIGHT"] ?: 1) as Int

        return world.body {
            type = BodyDef.BodyType.KinematicBody
            fixedRotation = true
            position.set(positionComp.x.toFloat(), positionComp.y.toFloat())
            box(width.toFloat(), height.toFloat()) {
                density = 10f
                filter {
                    categoryBits = FLOOR
                }
            }
        }
    }
}