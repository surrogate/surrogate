package com.gitlab.cvazer.surrogate.ecs

import com.gitlab.cvazer.surrogate.getBean

inline fun <reified T: Any> resource(res: T): T = resource(res, null)
inline fun <reified T: Any> resource(res: T, alias: String?): T {
    val clazz = T::class.java
    return getBean<EcsSystemResourceRegistry>().addResource(res, clazz, alias)
}

inline fun <reified T: Any> resource(): T {
    val clazz = T::class.java
    return getBean<EcsSystemResourceRegistry>().getResource(clazz)
}

fun <T: Any> resource(alias: String): T {
    return getBean<EcsSystemResourceRegistry>().getResource(alias)
}

fun <T: Any> resOrNull(alias: String): T? {
    return getBean<EcsSystemResourceRegistry>().getResource(alias)
}
