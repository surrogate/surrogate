package com.gitlab.cvazer.surrogate.game.atom.strategies.creation

import com.badlogic.ashley.core.Engine
import com.badlogic.ashley.core.Entity
import com.github.czyzby.autumn.annotation.Component
import com.github.czyzby.autumn.annotation.Inject
import com.gitlab.cvazer.surrogate.ecs.ComponentListeningEngine
import com.gitlab.cvazer.surrogate.ecs.components.*
import com.gitlab.cvazer.surrogate.game.atom.TargetingAtoms
import com.gitlab.cvazer.surrogate.game.atom.types.EntityStrategy
import com.gitlab.cvazer.surrogate.shared.model.Grid
import com.gitlab.cvazer.surrogate.shared.model.Pos
import ktx.ashley.get
import ktx.ashley.mapperFor

@Component
@TargetingAtoms("grid")
class GridEntityStrategy: EntityStrategy<Grid> {
    @Inject private lateinit var gameObjectEntityStrategy: GameObjectEntityStrategy
    @Inject private lateinit var pawnEntityStrategy: PawnEntityStrategy

    private val worldRendering = mapperFor<WorldRendering>()

    override fun toEntity(target: Grid, parent: Entity?, engine: Engine?): List<Entity> {
        if (engine == null) return emptyList()
        if (engine !is ComponentListeningEngine) return emptyList()

        val zLevel = parent
            ?.let { it[worldRendering] }
            ?.zLevel
            ?: 0

        val entity = engine.createEntity()
            .add { Id(target.id) }
            .add { AtomComp(target.atomLocator) }
            .add { GridWorld() }
            .add { Position(target.pos.x, target.pos.y) }
            .add { Parent(parent) }
            .add { Physics() }
            .add { Transform() }
            .add { WorldRendering(zLevel) }

        val pawnEntities = target.pawns
            .flatMap { pawnEntityStrategy.toEntity(it, entity, engine) }

        val maxLayer = mutableMapOf<Pos, Int>()
        target.objects.forEach {
            if (!maxLayer.containsKey(it.pos)) { maxLayer[it.pos] = it.layer; return@forEach }
            if (maxLayer[it.pos]!! < it.layer) maxLayer[it.pos] = it.layer
        }

        val objectEntities = target.objects
            .filter { it.layer == maxLayer[it.pos] }
            .flatMap { gameObjectEntityStrategy.toEntity(it, entity, engine) }

        return listOf(
            *pawnEntities.toTypedArray(),
            *objectEntities.toTypedArray(),
            entity
        )
    }
}