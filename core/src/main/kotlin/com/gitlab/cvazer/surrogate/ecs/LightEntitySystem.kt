package com.gitlab.cvazer.surrogate.ecs

import com.badlogic.ashley.core.*
import com.badlogic.gdx.utils.Array
import com.badlogic.gdx.utils.ObjectMap
import com.github.czyzby.autumn.annotation.Initiate
import com.gitlab.cvazer.surrogate.ecs.components.Id
import com.gitlab.cvazer.surrogate.ecs.event.Command
import com.gitlab.cvazer.surrogate.ecs.event.CommandReducer
import ktx.ashley.mapperFor
import ktx.collections.addAll
import ktx.collections.removeAll
import ktx.collections.set
import kotlin.reflect.KFunction
import kotlin.reflect.full.declaredFunctions
import kotlin.reflect.full.findAnnotation

abstract class LightEntitySystem(priority: Int = 10): EntitySystem(priority), EntityListener {
    private val reducersMap = ObjectMap<String, ReducerDescriptor>()
    val entities = Array<Entity>()

    @Initiate fun contextInit(){
        this::class.declaredFunctions.toList()
            .map { Pair(it, it.findAnnotation<CommandReducer>()) }
            .filter { it.second != null }
            .forEach { reducersMap[it.second!!.command] = ReducerDescriptor(it.first) }
    }

    protected val idMapper = mapperFor<Id>()

    override fun addedToEngine(engine: Engine?) {
        engine?.let { entities.addAll(it.getEntitiesFor(getFamilyBuilder().get())) }
        engine?.addEntityListener(getFamilyBuilder().get(), this)
        init()
    }

    override fun removedFromEngine(engine: Engine?) {
        engine?.let { entities.removeAll(it.getEntitiesFor(getFamilyBuilder().get())) }
        dispose()
    }

    override fun update(deltaTime: Float) {
        entities.forEach { updateEntity(it, deltaTime) }
        eventList.forEach {
            val reducer = reducersMap[it.command] ?: return@forEach
            reducer.func.call(this, it.value)
        }
    }

    fun notifyComponentWasAdded(entity: Entity) = entity
        .takeIf { getFamilyBuilder().get().matches(it) && !entities.contains(it, false) }
        ?.also { entities.add(it) }

    fun notifyComponentWasRemoved(entity: Entity) = entity
        .takeIf { getFamilyBuilder().get().matches(it) }
        ?.also { entities.removeValue(it, false) }

    override fun entityAdded(entity: Entity?) {
        entity
            ?.takeIf { !entities.contains(it, false) }
            ?.also { entities.add(entity) }
    }

    override fun entityRemoved(entity: Entity?) {
        entity?.let { entities.removeValue(entity, false); dispose(entity) }
    }

    fun onPlayerEntity(id: Id?, block: (id: String) -> Unit ) = id
        ?.let { resOrNull<String>("charId")?.takeIf { id.uuid == it }?.let {
            block.invoke(it)
        } }

    protected open fun init(){}
    protected open fun dispose(){}
    protected open fun dispose(entity: Entity?) {}

    protected open fun updateEntity(entity: Entity, delta: Float) {}
    abstract fun getFamilyBuilder(): Family.Builder

    data class ReducerDescriptor(val func: KFunction<*>)

    companion object {
        val eventList = Array<Command>()
    }
}