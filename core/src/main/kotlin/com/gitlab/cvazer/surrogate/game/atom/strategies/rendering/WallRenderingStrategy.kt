package com.gitlab.cvazer.surrogate.game.atom.strategies.rendering

import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.graphics.g2d.TextureAtlas
import com.badlogic.gdx.utils.Array
import com.badlogic.gdx.utils.ObjectMap
import com.github.czyzby.autumn.annotation.Component
import com.github.czyzby.autumn.annotation.Inject
import com.gitlab.cvazer.surrogate.ecs.EcsSystemResourceRegistry
import com.gitlab.cvazer.surrogate.ecs.components.*
import com.gitlab.cvazer.surrogate.game.GameUtils
import com.gitlab.cvazer.surrogate.game.atom.TargetingAtoms
import com.gitlab.cvazer.surrogate.game.atom.types.RenderingStrategy
import com.gitlab.cvazer.surrogate.game.getAllProps
import com.gitlab.cvazer.surrogate.shared.model.Atom
import com.gitlab.cvazer.surrogate.system.service.AssetManagerService
import ktx.ashley.get
import ktx.ashley.mapperFor

@Component
@TargetingAtoms("object.?.wall.?")
class WallRenderingStrategy: RenderingStrategy {
    @Inject private lateinit var assetManagerService: AssetManagerService
    @Inject private lateinit var ecsSystemResourceRegistry: EcsSystemResourceRegistry

    private val parentMapper = mapperFor<Parent>()
    private val idMapper = mapperFor<Id>()
    private val posMapper = mapperFor<Position>()

    override fun render(batch: SpriteBatch, atom: Atom, transform: Transform, propsComp: Props?, entity: Entity?) {
        val props = getAllProps(atom, propsComp)

        val width = (props["WIDTH"] as Int? ?: 1) * GameUtils.PIXELS_PER_UNIT
        val height = (props["HEIGHT"] as Int? ?: 1) * GameUtils.PIXELS_PER_UNIT

        val tilesAtlas = atom.resources
            .find { it.name == "tiles-atlas" }
            ?.let { assetManagerService.getAsset<TextureAtlas>(it.location()) } ?: return

        val objIndex: ObjectMap<String, Array<String>?> = ecsSystemResourceRegistry["gridObjPosIndex"]
        val gridId = entity?.get(parentMapper)?.entity?.get(idMapper)?.uuid ?: return
        val pos = entity[posMapper] ?: return

        val p1 = 0
        val p2 = if (fromIndex(objIndex, "$gridId|${pos.x-0}|${pos.y+1}").contains(atom.locator)) 1 else 0
        val p3 = 0
        val p4 = if (fromIndex(objIndex, "$gridId|${pos.x-1}|${pos.y-0}").contains(atom.locator)) 1 else 0
        val p5 = 1
        val p6 = if (fromIndex(objIndex, "$gridId|${pos.x+1}|${pos.y-0}").contains(atom.locator)) 1 else 0
        val p7 = 0
        val p8 = if (fromIndex(objIndex, "$gridId|${pos.x-0}|${pos.y-1}").contains(atom.locator)) 1 else 0
        val p9 = 0

        val region = tilesAtlas.findRegion("$p1$p2$p3-$p4$p5$p6-$p7$p8$p9")
        batch.draw(
            region,
            transform.x - width / 2,
            transform.y - height / 2,
            width, height
        )
    }
    
    private fun fromIndex(objIndex: ObjectMap<String, Array<String>?>, key: String): Array<String> {
        if (!objIndex.containsKey(key)) return Array()
        return objIndex[key]!!
    }
}