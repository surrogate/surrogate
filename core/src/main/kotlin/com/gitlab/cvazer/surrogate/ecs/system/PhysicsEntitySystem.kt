package com.gitlab.cvazer.surrogate.ecs.system

import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.core.Family
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.physics.box2d.Body
import com.github.czyzby.autumn.annotation.Component
import com.github.czyzby.autumn.annotation.Inject
import com.gitlab.cvazer.surrogate.ecs.LightEntitySystem
import com.gitlab.cvazer.surrogate.ecs.components.*
import com.gitlab.cvazer.surrogate.ecs.event.CommandReducer
import com.gitlab.cvazer.surrogate.ecs.event.EntityVelChangeData
import com.gitlab.cvazer.surrogate.game.GameUtils
import com.gitlab.cvazer.surrogate.game.atom.AtomStrategyRegistry
import com.gitlab.cvazer.surrogate.game.atom.types.PhysicsStrategy
import ktx.ashley.allOf
import ktx.ashley.exclude
import ktx.ashley.get
import ktx.ashley.mapperFor


@Component
class PhysicsEntitySystem: LightEntitySystem() {
    @Inject private lateinit var registry: AtomStrategyRegistry

    private val physicsMapper = mapperFor<Physics>()
    private val atomMapper = mapperFor<AtomComp>()
    private val transformMapper = mapperFor<Transform>()
    private val parentMapper = mapperFor<Parent>()
    private val positionMapper = mapperFor<Position>()
    private val worldMapper = mapperFor<GridWorld>()
    private val propsMapper = mapperFor<Props>()

    private var box2dCamera: OrthographicCamera? = null

    override fun updateEntity(entity: Entity, delta: Float) {
        val parent = entity[parentMapper]!!
//        val id = entity[idMapper]!!
        val body = entity[physicsMapper]!!
            .let { if (it.body != null) it.body!! else createBody(entity, it, parent) } ?: return

//        if (box2dCamera == null) { box2dCamera = resource("box2dDebugCamera") }

//        onPlayerEntity(id) { box2dCamera!!.position.set(
//            body.position.x,
//            body.position.y,
//            0f
//        ) }

        val parentBody = parent.entity?.let { it[physicsMapper] }?.body
        val pX = parentBody?.position?.x ?: 0f
        val pY = parentBody?.position?.y ?: 0f
        val x = (body.position.x + pX) * GameUtils.PIXELS_PER_UNIT
        val y = (body.position.y + pY) * GameUtils.PIXELS_PER_UNIT

        entity[transformMapper]!!.set(x, y)
    }

    private fun createBody(entity: Entity, physics: Physics, parent: Parent): Body? {
        val atom = entity[atomMapper]!!
        val position = entity[positionMapper]!!
        val props = entity[propsMapper]

        val world = parent.entity?.let { it[worldMapper] }?.world ?: return null
        physics.body = registry.getBestMatchingStrategy(atom, PhysicsStrategy::class)
            .create(world, position, atom, props)
        return physics.body
    }

    @CommandReducer("BODY_VELOCITY_CHANGE")
    fun bodyVelChangeReducer(data: EntityVelChangeData) {
        val target = (entities
            .map { Pair(it[idMapper], it) }
            .filter { it.first != null }
            .find { it.first!!.uuid == data.entityId }
            ?: return).second
        val body = (target[physicsMapper] ?: return).body ?: return
        body.linearVelocity = body.linearVelocity.add(data.vec)
    }

    override fun getFamilyBuilder(): Family.Builder = allOf(
        AtomComp::class, Transform::class,
        Physics::class, Position::class, Parent::class
    ).exclude(GridWorld::class)
}