package com.gitlab.cvazer.surrogate.backend

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.utils.Base64Coder
import com.github.czyzby.autumn.annotation.Component
import com.gitlab.cvazer.surrogate.readAs
import com.gitlab.cvazer.surrogate.shared.model.Atom
import com.gitlab.cvazer.surrogate.shared.model.Grid
import com.gitlab.cvazer.surrogate.shared.model.Resource
import com.gitlab.cvazer.surrogate.shared.model.ResourcePackage

@Component
class LocalFilesBackendApi: BackendApiAdapter() {

    override fun loadGrid(gridId: String): Grid = Gdx.files.local("data/grids/$gridId.json")
        .readString().readAs()

    override fun loadAtom(locator: String): Atom = Gdx.files.local(Atom.locatorToPath(locator))
        .readString().readAs()

    override fun loadResource(res: Resource): ResourcePackage =
        Gdx.files.local(res.location()).readBytes()
            .let { String(Base64Coder.encode(it)) }
            .let { ResourcePackage(res.path, res.fileName, it) }

    override fun loadCurrentChar(): String {
        return "LOCAL_SERVER_CHAR_ID"
    }
}