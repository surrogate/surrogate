package com.gitlab.cvazer.surrogate.game

import com.badlogic.gdx.utils.ObjectMap
import com.gitlab.cvazer.surrogate.ecs.components.AtomComp
import com.gitlab.cvazer.surrogate.ecs.components.Props
import com.gitlab.cvazer.surrogate.ecs.components.get
import com.gitlab.cvazer.surrogate.ecs.components.getAllProps
import com.gitlab.cvazer.surrogate.shared.model.Atom

object PhysicsCategories {
    const val PAWN   = 0x0000000000000001.toShort()
    const val FLOOR  = 0x0000000000000010.toShort()
    const val OBJECT = 0x0000000000000100.toShort()
}

object GameUtils {
    const val GRID_UNIT_TO_PIXELS = 128f
    const val PIXELS_PER_UNIT = 128f
}

object StrategyCategories {
    const val ENTITY_STRAT    = "ENTITY_STRATEGY"
    const val PHYSICS_STRAT   = "PHYSICS_STRAT"
    const val RENDERING_STRAT = "RENDERING_STRAT"
}

fun <K, V> ObjectMap<K, in V>.putIfAbsent(key: K, defValue: V) {
    if (!this.containsKey(key)) this.put(key, defValue)
}

fun getAllProps(atom: AtomComp, props: Props?) =
    props?.getAllProps(atom.get()) ?: emptyMap()
fun getAllProps(atom: Atom, props: Props?) =
    props?.getAllProps(atom) ?: emptyMap()