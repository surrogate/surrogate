package com.gitlab.cvazer.surrogate.game.atom.types

import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.gitlab.cvazer.surrogate.ecs.components.Props
import com.gitlab.cvazer.surrogate.ecs.components.Transform
import com.gitlab.cvazer.surrogate.game.StrategyCategories.RENDERING_STRAT
import com.gitlab.cvazer.surrogate.game.atom.AtomStrategy
import com.gitlab.cvazer.surrogate.shared.model.Atom

interface RenderingStrategy: AtomStrategy {
    fun render(batch: SpriteBatch, atom: Atom, transform: Transform, propsComp: Props?, entity: Entity?)
    fun render(batch: SpriteBatch, atom: Atom, transform: Transform, propsComp: Props?) =
        render(batch, atom, transform, propsComp, null)
    override fun getType(): String = RENDERING_STRAT
}