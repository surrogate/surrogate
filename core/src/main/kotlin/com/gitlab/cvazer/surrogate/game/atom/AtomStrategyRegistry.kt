package com.gitlab.cvazer.surrogate.game.atom

import com.badlogic.gdx.utils.Array
import com.badlogic.gdx.utils.ObjectMap
import com.github.czyzby.autumn.annotation.Component
import com.gitlab.cvazer.surrogate.ecs.components.AtomComp
import com.gitlab.cvazer.surrogate.game.putIfAbsent
import com.gitlab.cvazer.surrogate.shared.model.Atom
import com.gitlab.cvazer.surrogate.system.utils.ContextPostProcessor
import ktx.collections.set
import kotlin.reflect.KClass
import kotlin.reflect.full.allSuperclasses
import kotlin.reflect.full.allSupertypes
import kotlin.reflect.full.createType
import kotlin.reflect.full.findAnnotation

@Component
class AtomStrategyRegistry: ContextPostProcessor {
    private val stratRegexesByClass = ObjectMap<Class<*>, Array<Regex>?>()
    private val stratsByStratTypeClass = ObjectMap<Class<*>, Array<Any>>()

    private val stratsByAtomAndTypeBestMatch = ObjectMap<String, Any?>()

    inline fun <reified T: AtomStrategy> getBestMatchingStrategy(atom: AtomComp): T =
        getBestMatchingStrategy(atom, T::class)
    inline fun <reified T: AtomStrategy> getBestMatchingStrategy(atom: Atom): T =
        getBestMatchingStrategy(atom, T::class)
    inline fun <reified T: AtomStrategy> getBestMatchingStrategy(atom: String): T =
        getBestMatchingStrategy(atom, T::class)
    fun <T: AtomStrategy> getBestMatchingStrategy(atom: Atom, typeClass: KClass<T>): T =
        getBestMatchingStrategy(atom.locator, typeClass)
    fun <T: AtomStrategy> getBestMatchingStrategy(atom: AtomComp, typeClass: KClass<T>): T =
        getBestMatchingStrategy(atom.locator, typeClass)
    fun <T: AtomStrategy> getBestMatchingStrategy(atom: String, typeClass: KClass<T>): T {
        val key = "$atom|${typeClass.java.name}"
        val cached = stratsByAtomAndTypeBestMatch[key]
        @Suppress("UNCHECKED_CAST") if (cached != null) return cached as T

        val bestMatch = stratsByStratTypeClass[typeClass.java]?.map { strat ->
            val score = getBestMatchScore(strat::class, atom)
            Pair(strat, score)
        }?.maxByOrNull { it.second }?.first
        stratsByAtomAndTypeBestMatch[key] = bestMatch

        @Suppress("UNCHECKED_CAST") return bestMatch as T
    }

    private fun getBestMatchScore(stratClass: KClass<*>, atom: String): Int {
        val regexes = stratRegexesByClass[stratClass.java] ?: return 0
        val res = regexes.map { regex ->
            val res = regex.find(atom) ?: return@map null
            return@map res.groups.toList().size
        }.filterNotNull().maxOrNull() ?: 0
        return res
    }

    private fun processTargets(bean: Any, clazz: KClass<*>, ann: TargetingAtoms) {
        val regexes = ann.targets.toList().map { target ->
            target.split(".").joinToString(separator = "[.]", prefix = "^", postfix = "$") {
                when (it) {
                    "?" -> "(.*)"
                    "*" -> "([^.]+)"
                    else -> "(${it})"
                }
            }.toRegex()
        }
        stratRegexesByClass.put(clazz.java, Array(regexes.toTypedArray()))
    }

    override fun processBean(clazz: KClass<*>, bean: Any) {
        clazz.allSuperclasses
            .filter { it.allSupertypes.contains(AtomStrategy::class.createType()) }
            .onEach { stratsByStratTypeClass.putIfAbsent(it.java, Array()) }
            .forEach { stratsByStratTypeClass[it.java].add(bean) }
        clazz.findAnnotation<TargetingAtoms>()?.also { processTargets(bean, clazz, it) }
    }
}