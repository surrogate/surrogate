package com.gitlab.cvazer.surrogate.game

interface GameStage {
    fun init()
    fun tick(delta: Float)
    fun resize(width: Int, height: Int)
    fun dispose()
}