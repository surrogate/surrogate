package com.gitlab.cvazer.surrogate.game.atom.strategies.physics

import com.badlogic.gdx.physics.box2d.Body
import com.badlogic.gdx.physics.box2d.BodyDef
import com.badlogic.gdx.physics.box2d.World
import com.github.czyzby.autumn.annotation.Component
import com.github.czyzby.autumn.annotation.Inject
import com.gitlab.cvazer.surrogate.ecs.components.AtomComp
import com.gitlab.cvazer.surrogate.ecs.components.Position
import com.gitlab.cvazer.surrogate.ecs.components.Props
import com.gitlab.cvazer.surrogate.game.PhysicsCategories.OBJECT
import com.gitlab.cvazer.surrogate.game.PhysicsCategories.PAWN
import com.gitlab.cvazer.surrogate.game.atom.TargetingAtoms
import com.gitlab.cvazer.surrogate.game.atom.types.PhysicsStrategy
import com.gitlab.cvazer.surrogate.system.AtomsLoadingSystem
import ktx.box2d.body
import ktx.box2d.circle
import ktx.box2d.filter
import kotlin.experimental.or

@Component
@TargetingAtoms("pawn.?")
class PawnPhysicsStrategy: PhysicsStrategy {
    @Inject private lateinit var atomsLoadingSystem: AtomsLoadingSystem

    override fun create(world: World, positionComp: Position, atomComp: AtomComp, propsComp: Props?): Body {
//        val props = mutableMapOf<String, Any?>()
//            .apply { putAll(atomsLoadingSystem.getAtom(atomComp.locator)!!.atomProps) }
//            .apply { propsComp?.data?.also { this.putAll(it) } }
//
//        val width = (props["WIDTH"] ?: 1) as Int
//        val height = (props["WIDTH"] ?: 1) as Int

        return world.body(BodyDef.BodyType.DynamicBody) {
            fixedRotation = true
            position.set(positionComp.x.toFloat(), positionComp.y.toFloat())
            circle(0.5f) {
                density = 10f
                filter {
                    categoryBits = PAWN
                    maskBits = PAWN or OBJECT
                }
            }
        }
    }
}