package com.gitlab.cvazer.surrogate.game.stages

import com.badlogic.gdx.utils.ObjectMap
import com.badlogic.gdx.utils.Array
import com.github.czyzby.autumn.annotation.Component
import com.github.czyzby.autumn.annotation.Inject
import com.gitlab.cvazer.surrogate.Orchestrator
import com.gitlab.cvazer.surrogate.ecs.ComponentListeningEngine
import com.gitlab.cvazer.surrogate.ecs.EcsSystemResourceRegistry
import com.gitlab.cvazer.surrogate.ecs.LightEntitySystem
import com.gitlab.cvazer.surrogate.ecs.system.GridPhysicsEntitySystem
import com.gitlab.cvazer.surrogate.ecs.system.PhysicsEntitySystem
import com.gitlab.cvazer.surrogate.ecs.system.PlayerRotationEntitySystem
import com.gitlab.cvazer.surrogate.ecs.system.RenderingEntitySystem
import com.gitlab.cvazer.surrogate.game.GameStageAdapter
import com.gitlab.cvazer.surrogate.game.atom.AtomStrategyRegistry
import com.gitlab.cvazer.surrogate.game.atom.types.EntityStrategy
import com.gitlab.cvazer.surrogate.game.putIfAbsent
import com.gitlab.cvazer.surrogate.shared.model.Grid
import com.gitlab.cvazer.surrogate.system.AtomsLoadingSystem

@Component
class PlayGameStage: GameStageAdapter() {
    @Inject private lateinit var renderingEntitySystem: RenderingEntitySystem
    @Inject private lateinit var playerRotationEntitySystem: PlayerRotationEntitySystem
    @Inject private lateinit var physicsEntitySystem: PhysicsEntitySystem
    @Inject private lateinit var gridPhysicsEntitySystem: GridPhysicsEntitySystem

    @Inject private lateinit var ecsSystemResourceRegistry: EcsSystemResourceRegistry
    @Inject private lateinit var atomsLoadingSystem: AtomsLoadingSystem
    @Inject private lateinit var atomStrategyRegistry: AtomStrategyRegistry

    @Inject private lateinit var orchestrator: Orchestrator

    private lateinit var engine: ComponentListeningEngine
    private lateinit var grid: Grid

    override fun init() {
        engine = ComponentListeningEngine()
            .apply { addSystem(renderingEntitySystem.apply { priority = 7 }) }
            .apply { addSystem(playerRotationEntitySystem) }
            .apply { addSystem(gridPhysicsEntitySystem.apply { priority = 9 }) }
            .apply { addSystem(physicsEntitySystem.apply { priority = 8 }) }

        grid = orchestrator.backendApi.loadGrid("test-grid")
        ecsSystemResourceRegistry["charId"] = orchestrator.backendApi.loadCurrentChar()!!
        ecsSystemResourceRegistry["gridObjPosIndex"] = ObjectMap<String, Array<String>?>()
            .apply { getGridPosIndex(grid, this) }

        atomsLoadingSystem.loadAtom(grid.getAtomLocators())
        atomStrategyRegistry.getBestMatchingStrategy<EntityStrategy<Grid>>(grid.atomLocator)
            .toEntity(grid, null, engine)
    }

    private fun getGridPosIndex(grid: Grid, map: ObjectMap<String, Array<String>?>) {
        grid.objects.forEach {
                val key = "${grid.id}|${it.pos.x}|${it.pos.y}"
                map.putIfAbsent(key, Array())
                map[key]!!.add(it.atomLocator)
            }
        grid.children.forEach { getGridPosIndex(it, map) }
    }

    override fun tick(delta: Float) {
        if (!atomsLoadingSystem.allReady()) return
        engine.update(delta)
        LightEntitySystem.eventList.clear()
    }
}